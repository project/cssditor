<?php

namespace Drupal\custom_css_adder\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ThemeCssEditor. The Theme for the custom_css_adder module.
 *
 * @package Drupal\custom_css_adder\Form
 */
class ThemeCssEditor implements ThemeNegotiatorInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a Time object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return isset($stack->getCurrentRequest()->request->get('theme')) && isset($_SERVER['HTTP_REFERER']) &&
      $_SERVER['HTTP_REFERER'] == Url::fromUri('internal:/admin/appearance/settings/' . $stack->getCurrentRequest()->request->get('theme'), ['absolute' => TRUE])->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $stack->getCurrentRequest()->request->get('theme');
  }

}
